#!/usr/bin/env python

"""Simplified version of Linux ls command written in python

getopt module to parse command line arguments
Supports only option -l
"""

import sys
import getopt
import os
import datetime
import stat
import grp
import pwd


def print_file_detail(path, file_name):
    """
    Print function when option -l is used.
    """
    statinfo = os.stat(path + "/" + file_name)
    item_date = datetime.datetime.utcfromtimestamp(statinfo.st_mtime).strftime('%b %d %H:%M')
    uid = statinfo.st_uid
    gid = statinfo.st_gid
    user = pwd.getpwuid(uid)[0]
    group = grp.getgrgid(gid)[0]
    print("{} {} {} {} {} {} {}".format(stat.filemode(statinfo.st_mode), statinfo.st_nlink, user, group,
                                        statinfo.st_size, item_date, file_name))


def main(argv):
    """
    main function
    """

    # Parse arguments
    opt_l = False
    try:
        opts, _ = getopt.getopt(argv, "l")
    except getopt.GetoptError:
        print("ls.py [options] [file|dir] [file|dir]")
        sys.exit(1)
    for opt, _ in opts:
        if opt == '-l':
            opt_l = True

    # Print
    sorted_args = sorted(argv[len(opts):])
    if not sorted_args:
        # ls for local dir
        sorted_args = ["."]
    for item in sorted_args:
        if os.path.isfile(item):
            if opt_l:
                print_file_detail(".", item)
            else:
                print(item)
        elif os.path.isdir(item):
            if argv[len(opts):]:
                print("{}:".format(item))
            for f in sorted(os.listdir(item)):
                # do not display invisible file by default
                if f.startswith('.'):
                    continue
                if opt_l:
                    print_file_detail(item, f)
                else:
                    print(f, end="\t")
            if not opt_l:
                print()
        else:
            print("ls: {}: No such file or directory".format(item))


if __name__ == '__main__':
    main(sys.argv[1:])
