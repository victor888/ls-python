Écrire en Python une version simplifiée de ls sur Linux qui :
- liste les fichiers d'un dossier spécifié
- liste les fichiers d'un dossier commençant par un préfixe donné
- a une option "-l" pour afficher le mode et la date de création des fichiers

Le programme sera fourni avec des tests unitaire validant ces fonctionnalités (on pourra fournir un dossier de test avec des fichiers déjà créés pour faire passer les tests). Aucune autre fonctionnalité n'est attendue.

Exemples :

    ls.py /path/to/folder/
    some_file
    another_file

    ls.py /path/to/folder/som
    some_file

    ls.py -l /path/to/folder
    rwxr--r--  2017-12-15 17:44 some_file
    rwxr-xr-x  2017-12-15 17:44 another_file


Documentation :

    https://docs.python.org/3/library/os.html
    https://docs.python.org/3/library/stat.html
    https://docs.python.org/3/library/datetime.html
    https://docs.python.org/3/library/unittest.html

